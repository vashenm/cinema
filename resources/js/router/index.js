import { createRouter,createWebHistory } from "vue-router";
import CinemasIndex from "../components/Cinemas/CinemasIndex";
import CinemasCreate from "../components/Cinemas/CinemasCreate";
import CinemasEdit from "../components/Cinemas/CinemasEdit";
import TheatresIndex from "../components/Theatres/TheatresIndex";
import TheatresCreate from "../components/Theatres/TheatresCreate";
import TheatresEdit from "../components/Theatres/TheatresEdit";
import FilmsIndex from "../components/Films/FilmsIndex";
import FilmsCreate from "../components/Films/FilmsCreate";
import FilmEntry from "../components/Films/FilmEntry";
import ManageBooking from "../components/ManageBooking";

import Home from "../components/Home";

// import FilmsEdit from "../components/Films/FilmsEdit";
const routes = [{
    path:'/',
    name:'dashboard',
    component:Home
},
{
    path:'/cinemas',
    name:'cinemas.index',
    component:CinemasIndex
},
{
    path:'/cinemas/create',
    name:'cinemas.create',
    component:CinemasCreate
},
{
    path:'/cinemas/:id/edit',
    name:'cinemas.edit',
    component:CinemasEdit,
    props: true
},
{
    path:'/theatres',
    name:'theatres.index',
    component:TheatresIndex
},
{
    path:'/theatres/create',
    name:'theatres.create',
    component:TheatresCreate
},
{
    path:'/theatres/:id/edit',
    name:'theatres.edit',
    component:TheatresEdit,
    props: true
},
{
    path:'/films',
    name:'films.index',
    component:FilmsIndex
},
{
    path:'/films/create',
    name:'films.create',
    component:FilmsCreate
},
{
    path:'/film/:id',
    name:'film.entry',
    component:FilmEntry,
    props: true
},
{
    path:'/manage',
    name:'manage.booking',
    component:ManageBooking,
},

// {
//     path:'/films/:id/edit',
//     name:'films.edit',
//     component:FilmsEdit,
//     props: true
// }
]

export default createRouter({
    history:createWebHistory(),
    routes
})

