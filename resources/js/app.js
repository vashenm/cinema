require('./bootstrap');

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

import { createApp } from "vue";
import router from "./router";
import Cinemas from "./components/Cinemas/CinemasIndex";

createApp({
    components:{
        Cinemas    }
}).use(router).mount('#app')
