import {ref} from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function useTheatre(){
    const theatres = ref([])
    const theatre = ref([])
    const router = useRouter()
    const errors = ref('')

    const  getTheatres = async() => {
        let response = await axios.get('/api/theatres')
        theatres.value = response.data.data;
        console.log(theatres);
    }

    const getTheatre = async (id) => {
        let response = await axios.get('/api/theatres/' + id)
        theatre.value = response.data.data;
    }
    const storeTheatre = async (data) => {
        errors.value = ''
        try {
            await axios.post('/api/theatres', data)
            await router.push({name: 'theatres.index'})
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const updateTheatre = async (id) => {
        errors.value = ''
        try {
            await axios.put('/api/theatres/' + id, theatre.value)
            await router.push({name: 'theatres.index'})
        } catch (e) {
            if (e.response.status === 422) {
               errors.value = e.response.data.errors
            }
        }
    }

    const destroyTheatre = async (id) => {
        await axios.delete('/api/theatres/' + id)
    }
    return {
        theatres,
        theatre,
        errors,
        getTheatres,
        getTheatre,
        destroyTheatre,
        updateTheatre,
        storeTheatre
    }
}
