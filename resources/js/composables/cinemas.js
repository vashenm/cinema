import {ref} from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function useCinemas(){
    const cinemas = ref([])
    const cinema = ref([])
    const router = useRouter()
    const errors = ref('')

    const  getCinemas = async() => {
        let response = await axios.get('/api/cinemas')
        cinemas.value = response.data.data;
    }

    const getCinema = async (id) => {
        let response = await axios.get('/api/cinemas/' + id)
        cinema.value = response.data.data;
    }
    const storeCinema = async (data) => {
        errors.value = ''
        try {
            await axios.post('/api/cinemas', data)
            await router.push({name: 'cinemas.index'})
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const updateCinema = async (id) => {
        errors.value = ''
        try {
            await axios.put('/api/cinemas/' + id, cinema.value)
            await router.push({name: 'cinemas.index'})
        } catch (e) {
            if (e.response.status === 422) {
               errors.value = e.response.data.errors
            }
        }
    }

    const destroyCinema = async (id) => {
        await axios.delete('/api/cinemas/' + id)
    }
    return {
        cinemas,
        cinema,
        errors,
        getCinemas,
        getCinema,
        destroyCinema,
        updateCinema,
        storeCinema
    }
}
