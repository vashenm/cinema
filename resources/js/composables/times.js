import {ref} from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function useTime(){
    const times = ref([])
    const time = ref([])
    const router = useRouter()
    const errors = ref('')

    const  getTimes = async() => {
        let response = await axios.get('/api/times')
        console.log(response.data.data)
        times.value = response.data.data;
    }

    const getTime = async (id) => {
        let response = await axios.get('/api/times/' + id)
        time.value = response.data.data;
    }
    const storeTime = async (data) => {
        errors.value = ''
        console.log(data);
        try {
            await axios.post('/api/times', data)
            await router.push({name: 'times.index'})
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const updateTime = async (id) => {
        errors.value = ''
        try {
            await axios.put('/api/times/' + id, time.value)
            await router.push({name: 'times.index'})
        } catch (e) {
            if (e.response.status === 422) {
               errors.value = e.response.data.errors
            }
        }
    }

    const destroyTime = async (id) => {
        await axios.delete('/api/times/' + id)
    }
    return {
        times,
        time,
        errors,
        getTimes,
        getTime,
        destroyTime,
        updateTime,
        storeTime
    }
}
