import {ref} from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function useFilm(){
    const films = ref([])
    const film = ref([])
    const router = useRouter()
    const errors = ref('')

    const  getFilms = async() => {
        let response = await axios.get('/api/films')
        films.value = response.data.data;
    }

    const getFilm = async (id) => {
        let response = await axios.get('/api/films/' + id)
        film.value = response.data.data;
    }
    const storeFilm = async (data) => {
        errors.value = ''
        console.log(data);
        try {
            await axios.post('/api/films', data)
            await router.push({name: 'films.index'})
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const updateFilm = async (id) => {
        errors.value = ''
        try {
            await axios.put('/api/films/' + id, film.value)
            await router.push({name: 'films.index'})
        } catch (e) {
            if (e.response.status === 422) {
               errors.value = e.response.data.errors
            }
        }
    }

    const destroyFilm = async (id) => {
        await axios.delete('/api/films/' + id)
    }
    return {
        films,
        film,
        errors,
        getFilms,
        getFilm,
        destroyFilm,
        updateFilm,
        storeFilm
    }
}
