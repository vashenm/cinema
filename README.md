## Setup

- [composer install]
- [npm install]
- [php artisan storage:link]
- [php artisan migrate --seed]


## About
Please note I have not created a seeder for Films in order to demonstrate the create functionality.
To create please login with email: admin@admin.com and password: password and visit the /films/create to add films.

## Imporvements if I had more time
- [Roles to seperate user and admin]
- [complete crud for all entities]
- [Send mails for bookings]
- [Overall styling]