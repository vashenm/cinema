<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(1)->create();
        \App\Models\Cinema::factory(2)->create();
        \App\Models\Theatre::factory(2)->create(['cinema_id' => 1]);
        \App\Models\Theatre::factory(2)->create(['cinema_id' => 2]);
        \App\Models\Time::factory(1)->create(['start_time' => '08:00:00','end_time' => '10:00:00']);
        \App\Models\Time::factory(1)->create(['start_time' => '11:00:00','end_time' => '13:00:00']);
        \App\Models\Time::factory(1)->create(['start_time' => '14:00:00','end_time' => '16:00:00']);
        \App\Models\Time::factory(1)->create(['start_time' => '17:00:00','end_time' => '19:00:00']);
        \App\Models\Time::factory(1)->create(['start_time' => '20:00:00','end_time' => '22:00:00']);

    }
}
