<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->index();
            $table->integer('seats');
            $table->decimal('total', 9, 3);
            $table->boolean('confirmed')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('theatre_id');
            $table->unsignedBigInteger('film_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on("users");
            $table->foreign('theatre_id')->references('id')->on("theatres");
            $table->foreign('film_id')->references('id')->on("films");


        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
