<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_time', function (Blueprint $table) {
			$table->unsignedBigInteger('film_id')->unsigned()->index();
			$table->unsignedBigInteger('time_id')->unsigned()->index();

			$table->foreign('film_id')->references('id')->on('films')->onDelete('cascade');
			$table->foreign('time_id')->references('id')->on('times')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_time');
    }
}
