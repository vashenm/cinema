<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmTheatreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_theatre', function (Blueprint $table) {
			$table->unsignedBigInteger('film_id')->unsigned()->index();
			$table->unsignedBigInteger('theatre_id')->unsigned()->index();

			$table->foreign('film_id')->references('id')->on('films')->onDelete('cascade');
			$table->foreign('theatre_id')->references('id')->on('theatres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_theatre');
    }
}
