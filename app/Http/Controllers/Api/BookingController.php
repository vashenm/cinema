<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookingRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Booking;
use App\Models\Film;
use Illuminate\Http\Request;
use App\Http\Resources\FilmResource;
use App\Http\Resources\BookingResource;
use Illuminate\Support\Carbon;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookingRequest $request)
    {
        $film = Film::where('id', $request->film_id)->first();
        $booking = new Booking();
         $booking->time_id = $request->time_id;
         //improve by checking against table
         $booking->reference = $rand = substr(md5(microtime()),rand(0,26),6);
         $booking->seats = $request->seats;
         $booking->total = $request->seats * $film->price;
         $booking->user_id = $request->user_id;
         $booking->theatre_id = $request->theatre_id;
         $booking->film_id = $request->film_id;
         $booking->booking_date = $request->booking_date;

         $booking->save();

         return new BookingResource($booking);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }

    /**
     * Finds booking using reference
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function find($id){
        $booking = Booking::where('reference',$id)->with('theatre','film','time')->firstOrFail();
        return new BookingResource($booking);
    }

    /**
     * Cancels booking using id
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id){
        $booking = Booking::where('id',$id)->with('theatre','film','time')->firstOrFail();
        $now = Carbon::now();
        $date = Carbon::parse($now)->toDateString();

        if( $date != Carbon::create($booking->booking_date)->toDateString() || (time() - strtotime($booking->time->start_time) > 3599) && $date == Carbon::create($booking->booking_date)->toDateString() ){
            $booking->delete();
            return response()->json(['success' => 'success'], 200);
        }
       return response()->json(['status' => '500'], 500);
    }

    /**
     * Get available times for a day.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function times($filmId,$date){
        $available = [];
        $film = Film::where('id',$filmId)->firstOrFail();
        foreach($film->theatre as $theatre){
            $times = [];
            foreach($film->time as $time){
                $totalSeatsForTime = Booking::where('time_id',$time->id)->where('theatre_id',$theatre->id)->where('film_id',$film->id)->whereDate('booking_date',$date)->sum('seats');
                $remainingSeats = $theatre->seats - $totalSeatsForTime ;
                if($remainingSeats > 0 ){
                    //time is available
                    $times[] = array('id'=> $time->id,'start_time'=> $time->start_time,'end_time'=> $time->end_time,'remaining_seats'=>$remainingSeats);
                }
            }
            $available[] = array('id'=> $theatre->id,'name'=> $theatre->name,'times' =>$times );
            $film->setAttribute('available', $available);
        }

        return new FilmResource($film);
    }
}
