<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;
use App\Http\Requests\FilmRequest;
use App\Models\Film;
use App\Models\Booking;
use App\Http\Resources\FilmResource;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use PDO;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return FilmResource::collection(Film::all());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FilmRequest $request)
    {

        $film = new Film();
        $film->name = $request->name;
        $file = Storage::disk('public')->put('posters',$request->poster);
        $film->poster = $file;
        $film->price = $request->price;
        $film->restriction = $request->restriction ? $request->restriction :null;
        $film->length = $request->length ? $request->length :null;
        $film->byline = $request->byline ? $request->byline :null;

        $film->save();
        if($request->theatres){
            if (strpos($request->theatres, ',') !== false) {
                $explodedTheatres = explode(',',$request->theatres);
                foreach($explodedTheatres as $explodedTheatre){
                    $film->theatre()->attach( $explodedTheatre );
                }
            } else{
                $film->theatre()->attach( $request->theatres );
            }
        }

        if($request->times){
            if (strpos($request->times, ',') !== false) {
                $explodedTheatres = explode(',',$request->times);
                foreach($explodedTheatres as $explodedTheatre){
                    $film->time()->attach( $explodedTheatre );
                }
            } else{
                $film->time()->attach( $request->times );
            }
        }
        return new FilmResource($film);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function show(Film $film)
    {   
        //need to return the theatres and the times available with seat available per time
        //loop through theatres then loop through times check if booking with time theatre and time exits, if they do count and minus from threatre seats to show availble.
        $available = [];
        foreach($film->theatre as $theatre){
            $times = [];
            foreach($film->time as $time){
                $totalSeatsForTime = Booking::where('time_id',$time->id)->where('theatre_id',$theatre->id)->where('film_id',$film->id)->whereDate('created_at', Carbon::today())->sum('seats');
                $remainingSeats = $theatre->seats - $totalSeatsForTime ;
                if($remainingSeats > 0 ){
                    //time is available
                    $times[] = array('id'=> $time->id,'start_time'=> $time->start_time,'end_time'=> $time->end_time,'remaining_seats'=>$remainingSeats);
                }
            }
            $available[] = array('id'=> $theatre->id,'name'=> $theatre->name,'times' =>$times );
            $film->setAttribute('available', $available);
        }
        return new FilmResource($film);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function update(FilmRequest $request, Film $film)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function destroy(Film $film)
    {
        $film->delete();
        return response()->noContent();
    }
}
