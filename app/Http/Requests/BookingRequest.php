<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'time_id'=> ['required','exists:times,id'],
            'seats' =>['required','max:30'],
            'theatre_id' =>['required','exists:theatres,id'],
            'film_id' =>['required','exists:films,id'],
            'booking_date' =>['required'],
        ];
    }
}
