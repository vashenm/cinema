<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable =[
        'time_id',
        'reference',
        'seats',
        'total',
        'confirmed',
        'user_id',
        'theatre_id',
        'film_id',
        'booking_date'
    ];

    public function time()
    {
        return $this->belongsTo(Time::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function film()
    {
        return $this->belongsTo(Film::class);
    }

    public function theatre()
    {
        return $this->belongsTo(Theatre::class);
    }
}
