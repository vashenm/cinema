<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Theatre extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'cinema_id',
        'seats'
    ];

    protected $appends = ['cinemas'];

    public function cinema()
    {
        return $this->belongsTo(Cinema::class);
    }

    public function film()
    {
        return $this->hasOne(Film::class)->latestOfMany();
    }

    public function getCinemasAttribute()
    {
        return $this->cinema->name;
    }
}
