<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'poster',
        'price',
        'restriction',
        'length',
        'byline',
        'active',

    ];

    protected $appends = ['theatres','times'];

    public function getTheatresAttribute()
    {
        return $this->theatre;
    }
    public function getTimesAttribute()
    {
        return $this->time;
    }

    public function theatre()
    {
        return $this->belongsToMany(Theatre::class);
    }

    public function time()
    {
        return $this->belongsToMany(Time::class);
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

}
