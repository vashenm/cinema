<?php

use App\Http\Controllers\Api\BookingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');


require __DIR__.'/auth.php';


Route::group(['middleware' => 'auth'], function () {
    Route::view('/cinemas','dashboard');
    Route::view('/cinemas/create','dashboard');
    Route::view('/cinemas/edit','dashboard');
    Route::view('/cinemas/{id}/edit','dashboard');

    Route::view('/theatres','dashboard');
    Route::view('/theatres/create','dashboard');
    Route::view('/theatres/edit','dashboard');
    Route::view('/theatres/{id}/edit','dashboard');
    Route::view('/films','dashboard');
    Route::view('/films/create','dashboard');
    Route::view('/films/edit','dashboard');
    Route::view('/films/{id}/edit','dashboard');

});

Route::view('/home','dashboard');
Route::view('/manage','dashboard');
Route::get('/manage/booking/{id}',[BookingController::class, 'find']);
Route::get('/cancel/booking/{id}',[BookingController::class, 'cancel']);
Route::get('/film/{id}/times/{time}',[BookingController::class, 'times']);

