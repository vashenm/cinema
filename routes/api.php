<?php

use App\Http\Controllers\Api\BookingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('cinemas',\App\Http\Controllers\Api\CinemaController::class);
Route::apiResource('theatres',\App\Http\Controllers\Api\TheatreController::class);
Route::apiResource('films',\App\Http\Controllers\Api\FilmController::class);
Route::apiResource('times',\App\Http\Controllers\Api\TimeController::class);
Route::apiResource('bookings',\App\Http\Controllers\Api\BookingController::class);
